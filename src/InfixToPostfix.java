/**
 * @author CIS 121
 * 
 * An InfixToPostfix converter should take in equations in infix
 * form and convert them to postfix form.
 */
public class InfixToPostfix {

	/**
	 * This function should convert infix expressions to postfix expressions,
	 * implementing the standard Shunting Yard algorithm. It should convert
	 * any valid expression with the following operators:
	 *      + - * /
	 * any floating point numbers
	 * 
	 * @return space-delimited postfix expression
	 */
	public static String simpleShuntingYard(String input) {
            if((input == null) || input.equals("")){
                return "";
            }
            String output = "";
            DequeResizing<String> operators = new DequeResizing<String>();
            Tokenizer st = new Tokenizer(input);
            while(st.hasNextToken()){
                String currTok = st.getNextToken();
                if(isOperator(currTok)){
                    if(!operators.isEmpty()){
                        String top = operators.peek(); //top of stack
                        while(isOperator(top)){//if currTok has precendence <= to operator on top of the stack,
                            if(precedence(currTok) <= precedence(top)){
                                output = output.concat(operators.pop() + " "); //pop that operator to the output queue
                            } else {
                                break;
                            }
                            if(operators.isEmpty()){
                                break;
                            }
                            top = operators.peek();
                        }
                    }
                    operators.push(currTok); //...then push current operator onto the stack
                } else {
                    output = output.concat(currTok + " ");
                }
            }
            while(operators.size() > 1){
                output = output.concat(operators.pop() + " ");
            }
            
            output = output.concat(operators.pop());

            return output;
	}

	/**
	 * This function converts infix expressions to postfix expressions,
	 * implementing the standard Shunting Yard algorithm. It should convert
	 * any valid expression with the following operators:
	 *      + - * /
	 * any floating point numbers
	 * and any set of matching parentheses
	 * 
	 * @return space-delimited postfix expression
	 * @throws UnmatchedParenthesesException
	 */
	public static String mediumShuntingYard(String input) throws UnmatchedParenthesesException {
	    String output = "";
            if((input == null) || input.equals("")){
                return "";
            }
            DequeResizing<String> operators = new DequeResizing<String>();
            Tokenizer st = new Tokenizer(input);
            while(st.hasNextToken()){
                String currTok = st.getNextToken();
                
                if(isOperator(currTok)){
                    if(!operators.isEmpty()){
                        String top = operators.peek(); //top of stack
                        while(isOperator(top)){//if currTok has precendence <= to operator on top of the stack,
                            if(precedence(currTok) <= precedence(top)){
                                output = output.concat(operators.pop() + " "); //pop that operator to the output queue
                            } else {
                                break;
                            }
                            if(operators.isEmpty()){
                                break;
                            }
                            top = operators.peek();
                        }
                    }
                    operators.push(currTok); //...then push current operator onto the stack
                } else {
                    if(currTok.equals(")")){
                        if(!operators.contains("(")){
                            throw new UnmatchedParenthesesException();
                        }
                        while(!operators.peek().equals("(")){
                            if(operators.isEmpty()){
                                throw new UnmatchedParenthesesException();
                            }
                            output = output.concat(operators.pop() + " ");
                        }
                        operators.pop();
                    } else { 
                        if(currTok.equals("(")){
                            operators.push(currTok);
                        } else {
                            output = output.concat(currTok + " "); //otherwise, currTok is a float
                        }
                    }
                }
            }
            if(operators.contains("(") || operators.contains(")")){
                    throw new UnmatchedParenthesesException();
            }           
            while(operators.size() > 1){
                output = output.concat(operators.pop() + " ");
            }

            output = output.concat(operators.pop());
                
            return output;
	}

	/**
	 * This function converts infix expressions to postfix expressions,
	 * implementing the standard Shunting Yard algorithm. It should convert
	 * any valid expression with the following operators:
	 *      + - * / ^
	 * the following functions:
	 *      max(a,b) sin(a) cos(a)
	 * any floating point numbers
	 * and any set of matching parentheses
	 * 
	 * @return space-delimited postfix expression
	 * @throws UnmatchedParenthesesException
	 */
	public static String shuntingYard(String input) throws UnmatchedParenthesesException {
            String output = "";
            if((input == null) || input.equals("")){
                return "";
            }
            DequeResizing<String> operators = new DequeResizing<String>();
            Tokenizer st = new Tokenizer(input);
            while(st.hasNextToken()){
                String currTok = st.getNextToken();
                if(isOperator(currTok)){
                    if(!operators.isEmpty()){
                        String top = operators.peek(); //top of stack
                        while(isOperator(top)){//if currTok is left-associative and has equal precendence to an operator on top of the stack,
                            if((isLeftOperator(currTok) && (precedence(currTok) == precedence(top))) //or precendence of currTok is less than  
                                ||(precedence(currTok) < precedence(top))){//that of an operator on top of the stack,
                                output = output.concat(operators.pop() + " "); //pop that operator to the output queue
                            } else {
                                break;
                            }
                            if(operators.isEmpty()){
                                break;
                            }
                            top = operators.peek();
                        }
                    }
                    operators.push(currTok); //...then push current operator onto the stack
                } else {
                    if(currTok.equals(")")){
                        if(!operators.contains("(")){
                            throw new UnmatchedParenthesesException();
                        }
                        while(!operators.peek().equals("(")){
                            if(operators.isEmpty()){
                                throw new UnmatchedParenthesesException();
                            }
                            output = output.concat(operators.pop() + " ");
                        }
                        operators.pop();
                        if(!operators.isEmpty() && isFunction(operators.peek()) && operators.size() > 1){
                            output = output.concat(operators.pop() + " ");
                        }
                    } else {
                        if(currTok.equals("(") || isFunction(currTok)){
                            operators.push(currTok);//just push left parens and functions to stack
                        } else {
                            if(currTok.equals(",")){ //argument separator
                                String top = operators.peek();
                                while(!top.equals("(")){
                                    if(operators.isEmpty()){
                                        throw new UnmatchedParenthesesException();
                                    }
                                    output = output.concat(operators.pop() + " ");
                                    top = operators.peek();
                                }
                            } else {
                                output = output.concat(currTok + " "); //otherwise, currTok is a float; push to output
                            }
                        }
                    }
                }
            }    
            if(operators.contains("(") || operators.contains(")")){
                    throw new UnmatchedParenthesesException();
            }           
            
            while(operators.size() > 1){
                output = output.concat(operators.pop() + " ");
            }
            
            output = output.concat(operators.pop());
                
            return output;
	}
        
        private static boolean isLeftOperator(String s){
            return (s.equals("+")||s.equals("-")||s.equals("*")||s.equals("/"));
        }
        
        private static boolean isOperator(String s){
            return(isLeftOperator(s) || s.equals("^"));
        }
        
        private static boolean isFunction(String s){
            return (s.equals("max")||s.equals("sin")||s.equals("cos"));
        }
        
        private static int precedence(String op1) throws IllegalArgumentException{
            if(op1.equals("^")){
                return 4;
            }
            
            if(op1.equals("*")||op1.equals("/")){
                return 3;
            }
            
            if(op1.equals("+")||op1.equals("-")){
                return 2;
            }
            
            throw new IllegalArgumentException(); //if argument isn't an operator, it's illegal
        }
}
