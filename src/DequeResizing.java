
import java.util.Iterator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author martingreenberg
 */
public class DequeResizing<E> implements DequeI<E>, Iterable{
    
    private E[] dequeArr;
    private int dequeSize = 0;

    public DequeResizing(){
        this.dequeArr = (E[]) new Object[2];
    }

    @Override
    public boolean isEmpty() {
        return(dequeSize == 0);
    }

    @Override
    public int size() {
        return dequeSize;
    }

    @Override
    public boolean contains(Object o) {
        if((o==null) || this.isEmpty()){
            return false;
        }
        
        for(int i=0; i<this.size(); i++){
            if(dequeArr[i].equals(o)){
                return true;
            }
        }
        return false;
    }

    @Override
    public void offerFront(E elem) {
        E[] temp;
        if(dequeSize == dequeArr.length-1){
            temp = (E[]) new Object[dequeArr.length*2];
        } else {
            temp = (E[]) new Object[dequeArr.length];
        }
        temp[0] = elem;
        System.arraycopy(dequeArr, 0, temp, 1, dequeSize);
        dequeArr = temp;
        dequeSize++;
    }

    @Override
    public void offerBack(E elem) {
        E[] temp;
        if(dequeSize == dequeArr.length-1){
            temp = (E[]) new Object[dequeArr.length*2];
        } else {
            temp = (E[]) new Object[dequeArr.length];
        }
        System.arraycopy(dequeArr, 0, temp, 0, dequeSize);
        temp[dequeSize] = elem;
        dequeArr = temp;
        dequeSize++;
    }

    @Override
    public E pollFront() {
        E[] temp;
        if(this.isEmpty()){
            return null;
        }
        if(dequeSize-1 <= (dequeArr.length/4)){
            temp = (E[]) new Object[dequeArr.length/2];
        } else {
            temp = (E[]) new Object[dequeArr.length];
        }
        
        E toReturn = dequeArr[0];
        for(int i=1; i < dequeSize; i++){
            temp[i-1] = dequeArr[i];
        }
        dequeArr = temp;
        dequeSize--;
        return toReturn;
    }

    @Override
    public E pollBack() {
        E[] temp;
        if(this.isEmpty()){
            return null;
        }
        if(dequeSize-1 <= (dequeArr.length/4)){
            temp = (E[]) new Object[dequeArr.length/2];
        } else {
            temp = (E[]) new Object[dequeArr.length];
        }
        
        E toReturn = dequeArr[dequeSize-1];
        System.arraycopy(dequeArr, 0, temp, 0, dequeSize-1);
        dequeArr = temp;
        dequeSize--;
        return toReturn;
    }

    @Override
    public E peekFront() {
        if(this.isEmpty()){
            return null;
        }
        return dequeArr[0];
    }

    @Override
    public E peekBack() {
        if(this.isEmpty()){
            return null;
        }
        return dequeArr[dequeSize-1];
    }
    
    public E pop() {
        return this.pollFront();
    }
    
    public void push(E elem) {
        this.offerFront(elem);
    }
    
    public E peek(){
        return this.peekFront();
    }
    
    private class DequeResizingIterator implements Iterator<E>{
        private int nextIndex = 0;
        @Override
        public boolean hasNext(){
            return(nextIndex < dequeSize);
        }
        
        @Override
        public E next(){
            nextIndex++;
            return dequeArr[nextIndex-1];
        }
        
        @Override
        public void remove() throws UnsupportedOperationException{
            throw new UnsupportedOperationException();
        }
    }
    
    private class DequeResizingIteratorRev implements Iterator<E>{
        private int nextIndex = dequeSize-1;
        @Override
        public boolean hasNext(){
            return(nextIndex >= 0);
        }
        
        @Override
        public E next(){
            nextIndex--;
            return dequeArr[nextIndex+1];
        }
        
        @Override
        public void remove() throws UnsupportedOperationException{
            throw new UnsupportedOperationException();
        }
    }
    
    @Override
    public Iterator<E> iterator() {
        return new DequeResizingIterator();
    }

    @Override
    public Iterator<E> iteratorRev() {
        return new DequeResizingIteratorRev();
    }
}
