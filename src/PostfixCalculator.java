
import static java.lang.Float.parseFloat;

public class PostfixCalculator {

	/**
	 * Takes a postfix expression to be evaluated and outputs
	 * the calculated value.
	 * 
	 * @param expr Postfix expression to be evaluated
	 * @return Evaluation of the postfix expressions
	 * @throws IllegalArgumentException if the postfix expression is invalid
	 */
	public static float calculate(String expr) throws IllegalArgumentException {
            DequeResizing<Float> calcDeque = new DequeResizing<Float>();
            if(expr == null || expr.equals("")){
                throw new IllegalArgumentException();
            }
            
            Tokenizer st = new Tokenizer(expr);
            while(st.hasNextToken()){
                String currTok = st.getNextToken();
                
                if(currTok.equals("+")){
                    if(calcDeque.size() < 2){
                        throw new IllegalArgumentException();
                    }
                    calcDeque.push(calcDeque.pop()+calcDeque.pop());
                    continue;
                }
                
                if(currTok.equals("*")){
                    if(calcDeque.size() < 2){
                        throw new IllegalArgumentException();
                    }
                    calcDeque.push(calcDeque.pop()*calcDeque.pop());
                    continue;
                }
                
                if(currTok.equals("/")){
                    if(calcDeque.size() < 2){
                        throw new IllegalArgumentException();
                    }
                    float arg2 = calcDeque.pop();
                    float arg1 = calcDeque.pop();
                    calcDeque.push(arg1/arg2);
                    continue;
                }
                
                if(currTok.equals("-")){
                    if(calcDeque.size() < 2){
                        throw new IllegalArgumentException();
                    }
                    float arg2 = calcDeque.pop();
                    float arg1 = calcDeque.pop();
                    calcDeque.push(arg1-arg2);
                    continue;
                }
                
                if(currTok.equals("^")){
                    if(calcDeque.size() < 2){
                        throw new IllegalArgumentException();
                    }
                    double arg2 = (double)calcDeque.pop();
                    double arg1 = (double)calcDeque.pop();
                    calcDeque.push((float)Math.pow(arg1,arg2));
                    continue;
                }
                
                if(currTok.equals("max")){
                    if(calcDeque.size() < 2){
                        throw new IllegalArgumentException();
                    }
                    calcDeque.push(Math.max(calcDeque.pop(),calcDeque.pop()));
                    continue;
                }
                
                if(currTok.equals("sin")){
                    if(calcDeque.size() < 1){
                        throw new IllegalArgumentException();
                    }
                    calcDeque.push((float)Math.sin((double)calcDeque.pop()));
                    continue;
                }
                if(currTok.equals("cos")){
                    if(calcDeque.size() < 1){
                        throw new IllegalArgumentException();
                    }
                    calcDeque.push((float)Math.cos((double)calcDeque.pop()));
                    continue;
                }
                
                calcDeque.push(parseFloat(currTok));
            }
            
            if(calcDeque.size() != 1){
                throw new IllegalArgumentException();
            }
            return calcDeque.pop();
	}  
}
