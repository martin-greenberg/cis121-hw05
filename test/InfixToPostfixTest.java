/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author martingreenberg
 */
public class InfixToPostfixTest {
    
    public InfixToPostfixTest() {
    }
    
    @Before
    public void setUp(){
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSimpleShuntingYard1() { //test on standard input
        String exp = "2 3 + 4 1 * 3 / -";
        String actual = InfixToPostfix.simpleShuntingYard("2 + 3 - 4 * 1 / 3");
        assertEquals(exp, actual);
    }
    
    @Test
    public void testInvalidSimpleShuntingYard() { //empty or null expression
        assertEquals("",InfixToPostfix.simpleShuntingYard(""));
        assertEquals("",InfixToPostfix.simpleShuntingYard(null));
    }

    @Test
    public void testMediumShuntingYard1() throws UnmatchedParenthesesException {
        String exp = "2 3 4 * +";
        assertEquals(exp, InfixToPostfix.mediumShuntingYard("2 + ( 3 * 4 )"));
    }
    
    @Test
    public void testMediumShuntingYard2() throws UnmatchedParenthesesException {
        String exp = "2 3 4 * +";
        assertEquals(exp, InfixToPostfix.mediumShuntingYard("2 + 3 * 4"));
    }
    
    @Test
    public void testInvalidMediumShuntingYard1() throws UnmatchedParenthesesException{ //empty or null expression
        assertEquals("",InfixToPostfix.mediumShuntingYard(""));
        assertEquals("",InfixToPostfix.mediumShuntingYard(null));
    }
    
    @Test (expected=UnmatchedParenthesesException.class)
    public void testInvalidMediumShuntingYard2() throws UnmatchedParenthesesException { //too many right parens
        InfixToPostfix.mediumShuntingYard("( 2 + 3 ) )");
    }
    
    @Test (expected=UnmatchedParenthesesException.class)
    public void testInvalidMediumShuntingYard3() throws UnmatchedParenthesesException { //too many left parens
        InfixToPostfix.mediumShuntingYard("( ( 2 + 3 ) ");
    }

    @Test
    public void testShuntingYard1() throws UnmatchedParenthesesException {
        String exp = "2 3 4 * +";
        assertEquals(exp, InfixToPostfix.shuntingYard("2 + ( 3 * 4 )"));
    }
    
    @Test
    public void testShuntingYard2() throws UnmatchedParenthesesException {
        String exp = "2 3 4 * +";
        assertEquals(exp, InfixToPostfix.shuntingYard("2 + 3 * 4"));
    }
    
    @Test
    public void testShuntingYard3() throws UnmatchedParenthesesException {
        String exp = "2 3 4 * + sin";
        String actual = InfixToPostfix.shuntingYard("sin ( 2 + 3 * 4 )");
        assertEquals(exp, actual);
    }
    
    @Test
    public void testShuntingYard4() throws UnmatchedParenthesesException {
        String exp = "2 3 + 4 max";
        assertEquals(exp, InfixToPostfix.shuntingYard("max ( 2 + 3 , 4 )"));
    }
    
    @Test (expected=UnmatchedParenthesesException.class)
    public void testInvalidShuntingYard2() throws UnmatchedParenthesesException { //too many right parens
        InfixToPostfix.shuntingYard("( 2 + 3 ) )");
    }
    
    @Test (expected=UnmatchedParenthesesException.class)
    public void testInvalidShuntingYard3() throws UnmatchedParenthesesException { //too many left parens
        InfixToPostfix.shuntingYard("( ( 2 + 3 ) ");
    }
    
    @Test
    public void testInvalidShuntingYard4() throws UnmatchedParenthesesException{ //empty or null expression
        assertEquals("",InfixToPostfix.shuntingYard(""));
        assertEquals("",InfixToPostfix.shuntingYard(null));
    }
    
    @Test
    public void testShuntingYard5() throws UnmatchedParenthesesException{
        String exp = "2 2 cos 3 + * 7 / 5 +";
        assertEquals(exp, InfixToPostfix.shuntingYard("2 * ( cos ( 2 ) + 3 ) / 7 + 5"));
    }
}
