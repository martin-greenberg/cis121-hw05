/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author martingreenberg
 */
public class PostfixCalculatorTest {

    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testCalculateDiv() { //standard input with division
        float exp = (float) 0.5;
        assertEquals(PostfixCalculator.calculate("2 4 /"), exp, 0);
    }
    
    @Test
    public void testCalculateMult() { //standard input with mult
        float exp = (float) 8.0;
        assertEquals(PostfixCalculator.calculate("2 4 *"), exp, 0);
    }
    
    @Test
    public void testCalculateAdd() { //standard input with addition
        float exp = (float) 6.0;
        assertEquals(PostfixCalculator.calculate("2 4 +"), exp, 0);
    }
    
    @Test
    public void testCalculateSub() { //standard input with subtraction
        float exp = (float) -2.0;
        assertEquals(PostfixCalculator.calculate("2 4 -"), exp, 0);
    }
    
    @Test
    public void testCalculatePow() { //standard input with exponent
        float exp = (float) 16.0;
        assertEquals(PostfixCalculator.calculate("2 4 ^"), exp, 0);
    }
    
    @Test
    public void testCalculateMax() { //standard input with maximum
        float exp = (float) 4.0;
        assertEquals(PostfixCalculator.calculate("2 4 max"), exp, 0);
    }
    
    @Test
    public void testCalculateSin() { //standard input with sin
        float exp = (float) 0.0;
        assertEquals(PostfixCalculator.calculate("3.14159 sin"), exp, 0.1);
    }
    
    @Test
    public void testCalculateCos() { //standard input with cos
        float exp = (float) -1.0;
        assertEquals(PostfixCalculator.calculate("3.14159 cos"), exp, 0.1);
    }
    
    @Test
    public void testCalculateCompound(){ //standard input with combination of operators
        float exp = (float) 5.73824;
        assertEquals(PostfixCalculator.calculate("2 2 cos 3 + * 7 / 2 5 max +"), exp, 0.1);
    }
    
    @Test (expected = IllegalArgumentException.class)
    public void testCalculateTooManyOps() { //too many operators should throw IllegalArgumentException
        PostfixCalculator.calculate("2 4 * +");
    }
    
    @Test (expected = IllegalArgumentException.class)
    public void testCalculateNull() { //null input should throw IllegalArgumentException
        PostfixCalculator.calculate(null);
    }
    
    @Test (expected = IllegalArgumentException.class)
    public void testCalculateEmpty() { //empty input should throw IllegalArgumentException
        PostfixCalculator.calculate("");
    }
    
    @Test (expected = IllegalArgumentException.class)
    public void testCalculateTooManyConstants() { //too many constants should throw IllegalArgumentException
        PostfixCalculator.calculate("2 4 5 +");
    }
    
}
