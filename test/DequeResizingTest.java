/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.Iterator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author martingreenberg
 */
public class DequeResizingTest {
    
    public DequeResizingTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testIsEmpty1() { //test empty on an empty deque
        DequeResizing<Integer> testDeque = new DequeResizing<Integer>();
        assertTrue(testDeque.isEmpty());
    }
    
    @Test
    public void testIsEmpty2() {
        DequeResizing<Integer> testDeque = new DequeResizing<Integer>();
        testDeque.offerFront(1);
        assertFalse(testDeque.isEmpty());
    }

    @Test
    public void testSize() {
        DequeResizing<Integer> testDeque = new DequeResizing<Integer>();
        testDeque.offerFront(1);
        testDeque.offerFront(2);
        testDeque.offerFront(3);
        assertEquals(3, testDeque.size());
    }

    @Test
    public void testContains1() { //contains on a deque that doesn't contain the input
        DequeResizing<Integer> testDeque = new DequeResizing<Integer>();
        testDeque.offerFront(1);
        testDeque.offerFront(2);
        testDeque.offerFront(3);
        assertFalse(testDeque.contains(4));
    }
    
    @Test
    public void testContains2() { //contains on a deque that does contain the input
        DequeResizing<Integer> testDeque = new DequeResizing<Integer>();
        testDeque.offerFront(1);
        testDeque.offerFront(2);
        testDeque.offerFront(3);
        assertTrue(testDeque.contains(2));
    }


    @Test
    public void testOfferFront(){
        DequeResizing<Integer> testDeque = new DequeResizing<Integer>();
        testDeque.offerFront(1);
        testDeque.offerFront(2);
        testDeque.offerFront(3);
        assertEquals(testDeque.peekFront(),(Integer)3);
    }

    @Test
    public void testOfferBack() {
        DequeResizing<Integer> testDeque = new DequeResizing<Integer>();
        testDeque.offerBack(1);
        testDeque.offerBack(2);
        testDeque.offerBack(3);
        assertEquals(testDeque.peekFront(),(Integer)1);
    }

    @Test
    public void testPollFront1() {//pollFront on non-empty deque
        DequeResizing<Integer> testDeque = new DequeResizing<Integer>();
        testDeque.offerBack(1);
        testDeque.offerBack(2);
        testDeque.offerBack(3);
        assertEquals(testDeque.size(),3);
        assertEquals(testDeque.pollFront(),(Integer)1);
        assertEquals(testDeque.size(),2);
    }
    
    @Test
    public void testPollFront2() {//pollFront on empty deque should return null
        DequeResizing<Integer> testDeque = new DequeResizing<Integer>();
        assertEquals(testDeque.pollFront(),null);
    }

    @Test
    public void testPollBack1() {//pollBack on non-empty deque
        DequeResizing<Integer> testDeque = new DequeResizing<Integer>();
        testDeque.offerBack(1);
        testDeque.offerBack(2);
        testDeque.offerBack(3);
        assertEquals(testDeque.size(),3);
        assertEquals(testDeque.pollBack(),(Integer)3);
        assertEquals(testDeque.size(),2);
    }
    
    @Test
    public void testPollBack2() {//peekBack on empty deque should return null
        DequeResizing<Integer> testDeque = new DequeResizing<Integer>();
        assertEquals(testDeque.pollBack(),null);
    }

    @Test
    public void testPeekFront1() {//peekFront on non-empty deque
        DequeResizing<Integer> testDeque = new DequeResizing<Integer>();
        testDeque.offerBack(1);
        testDeque.offerBack(2);
        testDeque.offerBack(3);
        assertEquals(testDeque.peekFront(),(Integer)1);
    }
    
    @Test
    public void testPeekFront2() {//peekFront on empty deque should return null
        DequeResizing<Integer> testDeque = new DequeResizing<Integer>();
        assertEquals(testDeque.peekFront(),null);
    }

    @Test
    public void testPeekBack1() {//peekBack on non-empty deque
        DequeResizing<Integer> testDeque = new DequeResizing<Integer>();
        testDeque.offerBack(1);
        testDeque.offerBack(2);
        testDeque.offerBack(3);
        assertEquals(testDeque.peekBack(),(Integer)3);
    }
    
    @Test
    public void testPeekBack2() {//peekBack on empty deque should return null
        DequeResizing<Integer> testDeque = new DequeResizing<Integer>();
        assertEquals(testDeque.peekBack(),null);
    }
    @Test
    public void testIterator() {
        DequeResizing<Integer> testDeque = new DequeResizing<Integer>();
        testDeque.offerBack(1);
        testDeque.offerBack(2);
        testDeque.offerBack(3);
        Iterator<Integer> dequeIter = testDeque.iterator();
        assertTrue(dequeIter.hasNext());
        assertEquals(dequeIter.next(),(Integer)1);
        assertTrue(dequeIter.hasNext());
        assertEquals(dequeIter.next(),(Integer)2);
        assertTrue(dequeIter.hasNext());
        assertEquals(dequeIter.next(),(Integer)3);
        assertFalse(dequeIter.hasNext());
    }

    @Test
    public void testIteratorRev() {
        DequeResizing<Integer> testDeque = new DequeResizing<Integer>();
        testDeque.offerBack(1);
        testDeque.offerBack(2);
        testDeque.offerBack(3);
        Iterator<Integer> dequeIter = testDeque.iteratorRev();
        assertTrue(dequeIter.hasNext());
        assertEquals(dequeIter.next(),(Integer)3);
        assertTrue(dequeIter.hasNext());
        assertEquals(dequeIter.next(),(Integer)2);
        assertTrue(dequeIter.hasNext());
        assertEquals(dequeIter.next(),(Integer)1);
        assertFalse(dequeIter.hasNext());
    }
    
}
